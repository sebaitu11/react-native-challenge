import React, { useEffect } from 'react';
import {View, 
        StyleSheet, 
        Text,
        ImageBackground,
        Image,
        ScrollView,
        SafeAreaView} 
from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { HeaderButtons, Item } from 'react-navigation-header-buttons';
import * as detailActions from '../store/actions/detail';

import HeaderButton from '../components/UI/HeaderButton';

import HTMLView from 'react-native-htmlview';

const DetailScreen = props => {
  const job = props.navigation.getParam("job")
  const dispatch = useDispatch();
  const favorites = useSelector(state => state.detail.favorites)
  

  const favoriteJobHandler = async () => {
    await dispatch(detailActions.favoriteApi(job.id))
  }

  const unfavoriteJobHandler = async () => {
    await dispatch(detailActions.unfavoriteApi(job.id))
  }

  useEffect(() => { 
    props.navigation.setParams({ favoriteJob: favoriteJobHandler})
    props.navigation.setParams({ unfavoriteJob: unfavoriteJobHandler})
  },[]);

  useEffect(() => {
    const filtered_favorites = favorites.filter((fav) => { return fav.title === job.id })
    if(filtered_favorites.length > 0){
      props.navigation.setParams({ favorite: true})
    } else {
      props.navigation.setParams({ favorite: false})
    }
  }, [favorites])

  return (
      <ScrollView style={styles.screen}>
        <View>
          <ImageBackground source={require("../assets/texture.png")}
                           imageStyle={{resizeMode: "stretch"}}
                           style={{height: 150, width: "100%"}}
                           >
            <View style={styles.topInfoContainer}>
                <Text style={styles.title}>{job.title}</Text>
                { job.remote ? <Text style={styles.detailsText}>Trabajo Remoto - {job.modality}</Text> : <Text style={styles.detailsText}><Image style={{width: 20, height: 20}} source={{uri: job.country_flag_url}}/> {job.city}({job.country})- {job.modality}</Text>}
                <Text>{job.seniority} {job.salary ? "- $"  + job.salary +  "USD/Mes" : "" }</Text>
            </View>
          </ImageBackground>
          <View style={styles.descriptionContainer}>
            { job.functions ? 
              <View>
                <Text style={styles.subTitle}>Que harías</Text>
                <HTMLView value={job.functions} />
               </View>
            : null }

            { job.description ? 
              <View>
                <Text style={styles.subTitle}>Qué esperamos de ti</Text>
                <HTMLView value={job.description} />
              </View>
            : null }
            
            { job.benefits ? 
              <View>
                <Text style={styles.subTitle}>Qué deberías esperar de nosotros</Text>
                <HTMLView value={job.benefits} />
              </View>
            : null }
          </View>
        </View>
      </ScrollView>
  )
}

DetailScreen.navigationOptions = navData => {
  favorite = navData.navigation.getParam("favorite")
  if(favorite){
    return {
      headerRight: ( 
        <HeaderButtons HeaderButtonComponent={HeaderButton}>
          <Item
            title="Favorito"
            color="#f1c40f"
            iconName="ios-star"
            onPress={navData.navigation.getParam("unfavoriteJob")}
          />
        </HeaderButtons>
      )  
    }
  } else {
    return {
        headerRight: ( 
          <HeaderButtons HeaderButtonComponent={HeaderButton}>
            <Item
              title="Favorito"
              color="#ccc"
              iconName="ios-star"
              onPress={navData.navigation.getParam("favoriteJob")}
            />
          </HeaderButtons>
        )  
      }

  }
}

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    height: "100%",
  },
  title: {
    fontSize: 30,
    fontWeight: "bold",
    textAlign: "center"
  },
  topInfoContainer: {
    alignItems: "center",
    justifyContent: "center",
    height: 150
  },
  detailsText: {
    fontSize: 18,
  },
  descriptionContainer: {
    padding: 20,
    paddingTop: 0,
  },
  subTitle: {
    fontSize: 24, 
    fontWeight: "bold",
    marginVertical: 25,
  }
})

export default DetailScreen;