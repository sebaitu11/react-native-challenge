import React, { useState, useEffect } from 'react';
import {View, 
        StyleSheet, 
        Text,
        SafeAreaView,
        TextInput,
        FlatList,
        TouchableOpacity,
        Keyboard,
        Image,
        ActivityIndicator
        } 
from 'react-native';
import { useSelector, useDispatch } from 'react-redux';
import { Ionicons } from '@expo/vector-icons'
import * as homeActions from '../store/actions/home';
import * as detailActions from '../store/actions/detail';

const HomeScreen = props => {
  const jobs = useSelector(state => state.home.jobs)
  const favorites = useSelector(state => state.detail.favorites)
  const [query, setQuery] = useState("")
  const [showLoading, setShowLoading] = useState(false)
  const dispatch = useDispatch();


  const fetchFavorites = async () => {
    await dispatch(detailActions.getFavorites())
  }

  useEffect(() => { 
    fetchFavorites()
  },[]);


  const renderItem = (itemData)=> {
    return (
      <TouchableOpacity onPress={handleJobPress.bind(this, itemData.item)} >
        <View style={styles.jobContainer}>
          <View style={styles.imageContainer}>
            <Image style={{width: "100%", height: "100%"}} source={{uri: itemData.item.logo_url}} />
            { favorites.filter( fav => fav.title === itemData.item.id ).length > 0 ? <Ionicons color="#f1c40f" name="ios-star" size={30} style={styles.star} /> : null }
            
          </View>
          <View style={styles.dataContainer}>
            <Text style={styles.jobTitle}>{itemData.item.title}</Text>
            <Text style={styles.jobCompany}>{itemData.item.company.name}</Text>
            { itemData.item.salary ? <Text style={styles.salary}> $ {itemData.item.salary}USD/Mes </Text> : null  }
            { itemData.item.remote ? <Text style={styles.salary}>Trabajo Remoto</Text> : <Text style={styles.salary}><Image style={{width: 17, height: 17}} source={{uri: itemData.item.country_flag_url}}/>{itemData.item.city}({itemData.item.country})</Text>}
            
          </View>
        </View>
       </TouchableOpacity>
    );
  }

  const search = async () => {
    console.log("saerchhhhhh")
    try {
      Keyboard.dismiss()
      setShowLoading(true)
      await dispatch(homeActions.searchApi(query));
      setShowLoading(false)
    } catch (err) {
      console.log(err)
    }
    
  }

  const jobEmpty = () => {
    return (
      <View style={styles.emptyContainer}>
        <Text style={styles.emptyText}>Realiza una busqueda.</Text>
      </View>
    )
  }

  const handleSearchBox = (input) => {
    setQuery(input)
  }

  const handleJobPress = (job) => {
    props.navigation.navigate('Details', {job: job});
  }
  

  return (
    <SafeAreaView style={{ flex: 1, marginTop: 40 }}>
      <View style={styles.screenContainer} >
        <View style={ styles.search } >
          <Ionicons name="ios-search" size={20} style={{ marginRight: 10 }} />
          <TextInput
               underlineColorAndroid="transparent"
               placeholder="Buscar"
               placeholderTextColor="grey"
               style={{ flex: 1, fontWeight: "700", backgroundColor: "white" }}
               onChangeText={handleSearchBox}
             />
          <TouchableOpacity onPress={search}>
            <Text>Buscar</Text>
          </TouchableOpacity>
        </View>
      </View>
      { showLoading ? <View style={styles.centered}><ActivityIndicator size="large" color="black" /></View> 

      : <FlatList style={{flex: 1}}
                data={jobs}
                keyExtractor={item => item.id.toString()}
                renderItem={renderItem}
                ListEmptyComponent={jobEmpty}
      >
        
      </FlatList>
    }
      
    </SafeAreaView>
  )
}

HomeScreen.navigationOptions = {
  header: null,
};

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  screenContainer: {
    backgroundColor: "white",
    height: 60,
    borderBottomWidth: 1,
    borderBottomColor: "#dddddd"
  },
  search: {
    flexDirection: "row",
    padding: 10,
    backgroundColor: "white",
    marginHorizontal: 20,
    shadowOffset: { width: 0, height: 0 },
    shadowColor: "black",
    shadowOpacity: 0.2
  },
  jobContainer: {
    flexDirection: "row",
    borderBottomColor: "#ccc",
    borderBottomWidth: 1,
    paddingVertical: 10,
    paddingRight: 10,
  },
  imageContainer: {
    flex: 0.5,
    height: 100,
    marginRight: 20,
  },
  dataContainer: {
    flex: 1,
  },
  jobTitle: {
    fontSize: 18,
  },
  emptyContainer: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",

  },
  emptyText: {
    fontSize: 18,
    fontWeight: "bold",
    alignSelf: "center",
    marginTop: 20,
  },
  centered: {
    flex: 1,
    justifyContent: "center",
    alignItems: 'center',
  },
  star: {
    position: "absolute",
    bottom: 5,
    right: 5,
  }
})

export default HomeScreen;