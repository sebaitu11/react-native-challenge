import { createAppContainer  } from 'react-navigation';

import { createStackNavigator } from 'react-navigation-stack';
import React from 'react';
import {SafeAreaView, Button, View, Text} from 'react-native';

import { Ionicons } from '@expo/vector-icons'

import HomeScreen from '../screens/HomeScreen';
import DetailScreen from '../screens/DetailScreen';

const defaultNavOptions = {
  headerStyle: {
      backgroundColor: "#f1f1f1"
    },
  headerTintColor: '#333'
}


const MainNavigator = createStackNavigator({
  Home: HomeScreen,
  Details: DetailScreen,
},
  {
    defaultNavigationOptions: defaultNavOptions
  }
);

export default createAppContainer(MainNavigator)