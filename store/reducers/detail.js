
import { FAVORITE, UNFAVORITE, GET_FAVORITES } from '../actions/detail';

const initialState = {
  favorites: [],
};

export default (state = initialState, action) => {
  switch (action.type) {
    case FAVORITE:
      const updatedFavorites = [...state.favorites,  action.job]
      return {
        favorites: updatedFavorites
      }
    case UNFAVORITE:
      console.log("dasdsad")
      console.log(action.job)
      const updateFavorites = state.favorites.filter( favorite => favorite.title !== action.job.title )
      return {
        favorites: updateFavorites,
      }
    case GET_FAVORITES:
      console.log(action.favorites)
      return {
        favorites: action.favorites,
      }
  }

  return state;
};
