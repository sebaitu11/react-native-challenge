
import { SEARCH } from '../actions/home';

const initialState = {
  jobs: [],
};

export default (state = initialState, action) => {
  switch (action.type) {
    case SEARCH:
      console.log(action.jobs.jobs)
      return {
        jobs: action.jobs.jobs,
      }
  }

  return state;
};
