export const SEARCH = "SEARCH";

export const searchApi = (query) => {
    return async dispatch => {
      const response = await fetch(
        `https://www.getonbrd.com/search/jobs?q=${query}`,
        {
          method: 'GET',
          headers: {
            'Content-Type': 'application/json'
          }
        }
      );
  

      const resData = await response.json();
      console.log("RESDATA")

      dispatch({type: SEARCH, jobs: resData})
      
    };
  };
