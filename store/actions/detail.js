export const FAVORITE = "FAVORITE";
export const UNFAVORITE = "UNFAVORITE";
export const GET_FAVORITES = "GET_FAVORITES";

export const getFavorites = () => {
  return async dispatch => {
    const response = await fetch(
      `https://getonbrd-challenge.herokuapp.com/favorite_jobs`,
      {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json'
        }
      }
    );

    const resData = await response.json();
    dispatch({type: GET_FAVORITES, favorites: resData.favorites})
    
  };
};



export const favoriteApi = (id) => {
  return async dispatch => {
    const response = await fetch(
      `https://getonbrd-challenge.herokuapp.com/favorite_jobs/favorite?title=${id}`,
      {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        }
      }
    );

    const resData = await response.json();
    dispatch({type: FAVORITE, job: resData.job})
    
  };
};

export const unfavoriteApi = (id) => {
  return async dispatch => {
    const response = await fetch(
      `https://getonbrd-challenge.herokuapp.com/favorite_jobs/${id}/unfavorite`,
      {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        }
      }
    );

    const resData = await response.json();

    dispatch({type: UNFAVORITE, job: resData.job})
    
  };
};


