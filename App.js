import { createStore, combineReducers, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import ReduxThunk from 'redux-thunk'
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

import MainNavigator from './navigation/MainNavigator';
import homeReducer  from './store/reducers/home'
import DetailReducer  from './store/reducers/detail'

const rootReducer = combineReducers({
  home: homeReducer,
  detail: DetailReducer
})

const store = createStore(rootReducer, applyMiddleware(ReduxThunk))

export default function App() {
  return (
    <Provider store={store}>
      <MainNavigator />
    </Provider>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
